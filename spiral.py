#!/usr/bin/python

"CS244 Assignment 3: Downward spiral effect in commercial video streaing service"

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

import subprocess
from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
import termcolor as T
from argparse import ArgumentParser

from matplotlib.font_manager import FontProperties

import sys
import os
import math
import numpy as np
import operator

import matplotlib.pyplot as plt


#from util.monitor import monitor_qlen
#from util.helper import stdev




def cprint(s, color, cr=True):
    """Print in color
        s: string to print
        color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),


# Parse arguments

parser = ArgumentParser(description="Downward spiral effect tests")
parser.add_argument('--bw-host', '-B',
                    dest="bw_host",
                    type=float,
                    action="store",
                    help="Bandwidth of host links",
                    required=True)

parser.add_argument('--bw-net', '-b',
                    dest="bw_net",
                    type=float,
                    action="store",
                    help="Bandwidth of network link",
                    required=True)

parser.add_argument('--delay',
                    dest="delay",
                    type=float,
                    help="Delay in milliseconds of host links",
                    default=1)

parser.add_argument('--dir', '-d',
                    dest="dir",
                    action="store",
                    help="Directory to store outputs",
                    default="results",
                    required=True)

parser.add_argument('-n',
                    dest="n",
                    type=int,
                    action="store",
                    help="Number of nodes in star.  Must be >= 3",
                    required=True)

parser.add_argument('--maxq',
                    dest="maxq",
                    action="store",
                    help="Max buffer size of network interface in packets",
                    default=80)

parser.add_argument('--cong',
                    dest="cong",
                    help="Congestion control algorithm to use",
                    default="bic")

parser.add_argument('--time', '-t',
                    help="Duration (sec) to run the experiment",
                    type=int,
                    default=900)

parser.add_argument('--conservative',
                    help="Ratio of conservativeness - ratio of rate chosen to the measured bandwidth should be less than conservative value. Put conservative = 0 for Netflix type client",
                    type=float,
                    default=0)


parser.add_argument('--PLAYBACK_BUFFER',
                    help="Playback buffer size in seconds. Default value is 240 seconds",
                    type=int,
                    default=240)

parser.add_argument('--SEGMENT_SIZE',
                    help="Video segment size in seconds. Default value is 4 seconds",
                    type=int,
                    default=4)

parser.add_argument('--comp_flow_start_time',
                    help="Time in seconds at which the competing flow starts after the flow starts",
                    type=int,
                    default=400)

parser.add_argument('--COMP_FLOW_STAYS',
                    help="Time in seconds for which competing flow stays alive",
                    type=int,
                    default=400)

parser.add_argument('--FILTER_LEN',
                    help="Number of preceeding file downloads to be considered for estimating throughput",
                    type=int,
                    default=10)

parser.add_argument('--n_ave',
                    help="Length of moving average filter to smooth the output displayed on graphs",
                    type=int,
                    default=5)


# Expt parameters
args = parser.parse_args()
#print "Hello"
#print args
#print args.conservative
#print args.time

print args.maxq
filename_to_save = "%s/conservativeRatio%.1f-qsize%s.png"%(args.dir,args.conservative,args.maxq)
cprint ("saving results to figure file %s"% filename_to_save, 'green')
args.maxq = float(args.maxq)
args.maxq = (args.maxq*1000)/1500
#TIME_DIFF = 0.3
#FILTER_TIME = 15
PLAYBACK_BUFFER = args.PLAYBACK_BUFFER
SEGMENT_SIZE = args.SEGMENT_SIZE
comp_flow_start_time = args.comp_flow_start_time
COMP_FLOW_STAYS = args.COMP_FLOW_STAYS
FILTER_LEN = args.FILTER_LEN
n_ave = args.n_ave

# Some more variables for verification of bandwidth
# Number of samples to skip for reference util calibration.
CALIBRATION_SKIP = 10
# Number of samples to grab for reference util calibration.
CALIBRATION_SAMPLES = 30
# Fraction of input bandwidth required to begin the experiment.
# At exactly 100%, the experiment may take awhile to start, or never start,
# because it effectively requires waiting for a measurement or link speed
# limiting error.
START_BW_FRACTION = 0.9
# Number of samples to take in get_rates() before returning.
NSAMPLES = 3
# Time to wait between samples, in seconds, as a float.
SAMPLE_PERIOD_SEC = 1.0
# Time to wait for first sample, in seconds, as a float.
SAMPLE_WAIT_SEC = 3.0


#Create the directory for results
if not os.path.exists(args.dir):
    os.makedirs(args.dir)

lg.setLogLevel('info')

# Topology to be instantiated in Mininet
class StarTopo(Topo):
    "Star topology for Downward spiral effect experiment"
    
    def __init__(self, n=3, cpu=None, bw_host=None, bw_net=None,
                 delay=None, maxq=None):
        # Add default members to class.
        super(StarTopo, self ).__init__()
        self.n = n
        self.cpu = cpu
        self.bw_host = bw_host
        self.bw_net = bw_net
        self.delay = delay
        self.maxq = maxq
        self.create_topology()
    
    #Creating topology for experiments
    def create_topology(self):
        # Here h1 and h2 are hosts and h3 is the server
        switch = self.addSwitch('s0')
        
        host = self.addHost('h3')
        self.addLink(host, switch,
                     bw=args.bw_net, delay="%f%s" % (args.delay, 'ms'),max_queue_size = args.maxq)

        
        for i in range(1,args.n):
            host = self.addHost('h%d' % i)
            self.addLink(host, switch,
                         bw=args.bw_host, delay="%f%s" % (args.delay, 'ms'),max_queue_size = args.maxq)
        


def start_tcpprobe():
    "Install tcp_probe module and dump to file"
    os.system("rmmod tcp_probe 2>/dev/null; modprobe tcp_probe;")
    Popen("cat /proc/net/tcpprobe > %s/tcp_probe.txt" %
          args.dir, shell=True)


def start_tcpprobe_for_bw():
    "Install tcp_probe module and dump to file"
    os.system("rmmod tcp_probe 2>/dev/null; modprobe tcp_probe;")
    Popen("cat /proc/net/tcpprobe > %s/tcp_probe_for_bw.txt" %
          args.dir, shell=True)


def stop_tcpprobe():
    os.system("killall -9 cat; rmmod tcp_probe &>/dev/null;")

def get_txbytes(iface):
    f = open('/proc/net/dev', 'r')
    lines = f.readlines()
    for line in lines:
        if iface in line:
            break
    f.close()
    if not line:
        raise Exception("could not find iface %s in /proc/net/dev:%s" %
                        (iface, lines))
    # Extract TX bytes from:
    #Inter-|   Receive                                                |  Transmit
    # face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
    # lo: 6175728   53444    0    0    0     0          0         0  6175728   53444    0    0    0     0       0          0
    return float(line.split()[9])

def get_rates(iface, nsamples=NSAMPLES, period=SAMPLE_PERIOD_SEC,
              wait=SAMPLE_WAIT_SEC):
    """Returns the interface @iface's current utilization in Mb/s.  It
        returns @nsamples samples, and each sample is the average
        utilization measured over @period time.  Before measuring it waits
        for @wait seconds to 'warm up'."""
    # Returning nsamples requires one extra to start the timer.
    nsamples += 1
    last_time = 0
    last_txbytes = 0
    ret = []
    sleep(wait)
    while nsamples:
        nsamples -= 1
        txbytes = get_txbytes(iface)
        now = time()
        elapsed = now - last_time
        #if last_time:
        #    print "elapsed: %0.4f" % (now - last_time)
        last_time = now
        # Get rate in Mbps; correct for elapsed time.
        rate = (txbytes - last_txbytes) * 8.0 / 1e6 / elapsed
        if last_txbytes != 0:
            # Wait for 1 second sample
            ret.append(rate)
        last_txbytes = txbytes
        print '.',
        sys.stdout.flush()
        sleep(period)
    return ret


def avg(s):
    "Compute average of list or string of values"
    if ',' in s:
        lst = [float(f) for f in s.split(',')]
    elif type(s) == str:
        lst = [float(s)]
    elif type(s) == list:
        lst = s
    return sum(lst)/len(lst)

def median(l):
    "Compute median from an unsorted list of values"
    s = sorted(l)
    if len(s) % 2 == 1:
        return s[(len(l) + 1) / 2 - 1]
    else:
        lower = s[len(l) / 2 - 1]
        upper = s[len(l) / 2]
        return float(lower + upper) / 2

def format_floats(lst):
    "Format list of floats to three decimal places"
    return ', '.join(['%.3f' % f for f in lst])

def ok(fraction):
    "Fraction is OK if it is >= args.target"
    return fraction >= args.target

def format_fraction(fraction):
    "Format and colorize fraction"
    if ok(fraction):
        return T.colored('%.3f' % fraction, 'green')
    return T.colored('%.3f' % fraction, 'red', attrs=["bold"])


def verify_latency(net,src):
    " verify link latency"
    host = net.getNodeByName('h%d' % src)
    h3 = net.getNodeByName('h3')
    #    host.popen("ping -c5 -q -i 0.1 %s > %s/%s" % (h3.IP(),args.dir, "pingh1h3.txt"),shell=True)
    #    sleep(1)
    latency = host.popen("ping -c5 -i 0.1 %s | grep rtt | cut -d\"/\" -f5" % (h3.IP()), shell = True, stdout=PIPE)
    sleep(1)
    avg_latency = latency.stdout.read()
    return (float(avg_latency)/(4*args.delay) <= 1.1)


def verify_bandwidth(net,src,des):
    "verify bandwidth"
    #  src is the sender (client). des is destination (server)
    print "Verifying bandwidth..."
    # Start iperf server.
    server = net.getNodeByName('h%d' % des)
    print "Starting iperf server..."
    s = server.popen("iperf -s -w 16m")
    
    client = net.getNodeByName('h%d' % src)
    print "Starting iperf client..."
    c = client.popen("iperf -c %s -t 3600" % (server.IP()))
    
    port_to_watch = (des+1)%3   # if destination is 3, port to watch is 1. If destination is 2 (in case we want to verify bw_host), port to watch is 3.
    print port_to_watch
    iface = 's0-eth%d' % port_to_watch
#    iface = 's0-eth1'
    rates = get_rates(iface, nsamples=CALIBRATION_SAMPLES+CALIBRATION_SKIP)
    rates = rates[CALIBRATION_SKIP:]
    med_rate = avg(rates)
    # Shut down iperf processes
    os.system('killall -9 iperf')
    return ( (med_rate) )



def check_buffer(rate_request,file_size,fetch_time,buffer_occu):
    "Query the buffer size after each file download. The buffer size drains out by amount equal to time taken for file download. The buffer size increases by SEGMENT_SIZE"
    bytes_inflow = float(file_size)
    time_inflow = SEGMENT_SIZE
    time_outflow = float(fetch_time)
    buffer_used = max(float(buffer_occu) - time_outflow + time_inflow,0)
    buffer_available = PLAYBACK_BUFFER - buffer_used
    return buffer_available


#def get_file(net,file_size):
#    h3 = net.getNodeByName('h3')
#    h1 = net.getNodeByName('h1')
#    file_size = int((file_size*1e6)/8) # converting file_size in Mb to bytes
#    fetch_time = h1.cmd("curl --keepalive-time %d -r 0-%d -o /dev/null -s -w %s %s/%s " % (1,file_size-1,"%{time_total}",h3.IP(),"http/ready.html"))
#    return fetch_time

def get_indices(tot_time,FILTER_TIME):
    "Function used for debugging"
    for i, e in reversed(list(enumerate(tot_time))):
        if (tot_time[-1]-tot_time[i] > FILTER_TIME):
            break
    indices = i+1
    return indices

def get_intervals(tot_time,thrput_indices,FILTER_TIME):
    "Function used for debugging"
    intervals = []
    for i in range(thrput_indices+1,len(tot_time)):
        intervals.append( tot_time[i] - tot_time[i-1] )
    time_recovered = sum(intervals)
    intervals.insert(0,FILTER_TIME-time_recovered)
    return intervals

#def get_adjusted_thrput(thrput_tot,tot_time,FILTER_TIME):
#    if len(thrput_tot) == 0:
#        throughput = args.bw_net
#    elif tot_time[-1]<FILTER_TIME:
#        throughput = sum(thrput_tot)/len(thrput_tot)
##        print sum(thrput_tot)
##        print len(thrput_tot)
#    else:
#        thrput_indices = get_indices(tot_time,FILTER_TIME)
#        time_vec = [float(i) for i in get_intervals(tot_time,thrput_indices,FILTER_TIME)]
#        thrput_vec = thrput_tot[thrput_indices:]
#        avg_thrput = reduce( operator.add, map( operator.mul, time_vec, thrput_vec))
#        throughput = avg_thrput/FILTER_TIME
#    
##    print ("throughput is %f"%throughput)

def get_adjusted_thrput(thrput_tot,FILTER_LEN):
    "Function used for debugging"
    if len(thrput_tot) == 0:
        throughput = args.bw_net
    elif len(thrput_tot)<FILTER_LEN:
        throughput = sum(thrput_tot)/len(thrput_tot)
    else:
        throughput = sum(thrput_tot[-FILTER_LEN:])/len(thrput_tot[-FILTER_LEN:])

    return throughput


def choose_rate(thrput_tot,tot_time):
    "Pick up video rate on observed throughput. Average the rate of downloads for last FILTER_LEN segments and estimate the throughput. The algorithm for selecting rate based on this throuput depends on <CONSERVATISM> input. By default, algorithm used by commercial video service is implemented. "
    
    #    throughput = get_adjusted_thrput(thrput_tot,tot_time,FILTER_TIME)
    # initial throughput put at maximum so that maximum playback rate is used
    if len(thrput_tot) == 0:
        throughput = args.bw_net
    elif len(thrput_tot)<FILTER_LEN:
        throughput = sum(thrput_tot)/len(thrput_tot)
    else:
        throughput = sum(thrput_tot[-FILTER_LEN:])/len(thrput_tot[-FILTER_LEN:])
    
    # algorithm used by commercial video service
    if args.conservative == 0:
        if throughput>2.5:
            rate_request = 1.75
        elif throughput>2.00:
            rate_request = 1.4
        elif throughput>1.20:
            rate_request = 1.05
        elif throughput>0.95:
            rate_request = 0.75
        elif throughput>0.6:
            rate_request = 0.56
        elif throughput>0.40:
            rate_request = 0.375
        else:
            rate_request = 0.235
    # algorithm whihch can support non-discrete bit-rates
    else:
        rate_request = args.conservative*throughput

    return rate_request

def start_webserver(net):
    "Starting the web server on host h3. It contains a file requested by host h1"
    h3 = net.getNodeByName('h3')
    proc = h3.popen("python http/RangeHTTPServer.py", shell=True)
    sleep(1)
    return [proc]

def get_wait_time(buffer_available):
    "If uffer available is less than SEGMENT_SIZE, wait till the buffer gets sufficiently drained before requesting for new file"
    if (buffer_available>SEGMENT_SIZE):
        wait_time = 0
    else:
        wait_time = SEGMENT_SIZE - buffer_available
    return wait_time

def start_iperf(net):
    "Iperf provies competing flow"
    h2 = net.getNodeByName('h2')
    h3 = net.getNodeByName('h3')
    print "Starting iperf server..."
    server = h2.popen("iperf -i  1 -s -t %d -w 16m | awk '/Mbits\/sec/ ' > compflow_download_speed_server_side.txt" %COMP_FLOW_STAYS,shell = True)
    client = h3.popen("iperf -i  1 -c %s -t %d -w 16m > compflow_download_speed.txt " %(h2.IP(), COMP_FLOW_STAYS),shell = True)


def plot_inst_thr(tot_time,thrput_tot,rate_request_tot,compflow_bw):
    "Code for plotting final plots"
    time_for_comp_flow = range(comp_flow_start_time,comp_flow_start_time+len(compflow_bw))
    #    print len(time_for_comp_flow)
    #    print len(compflow_bw)
    plt.plot(tot_time,thrput_tot,'-b',tot_time,rate_request_tot,'-r',time_for_comp_flow,compflow_bw,'-g' )
    plt.ylabel('in Mbps')
    plt.xlabel('time in seconds')
    fontP = FontProperties()
    fontP.set_size('small')
    plt.legend(('video throughput', 'video bit rate','competing flow throughput'), prop = fontP, bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
    for y_value in [1.75, 1.4, 1.05, 0.75, 0.56, 0.375, 0.235]:
        plt.axhline(y=y_value,color = '0.8')

    #    if args.conservative == 0:
    #        filename_to_save = "%s/conservativeRatio%s-qsize%d.png"%(args.dir,"Netflix",args.maxq)
    #    else:
    #        filename_to_save = "%s/conservativeRatio%.1f-qsize%d.png"%(args.dir,args.conservative,int(args.maxq*1500/1000))
    plt.savefig("%s"%filename_to_save)


def smooth(thrput_tot,n_ave):
    "Filter the values using moving average filter of length n_ave"
    smoothed_thrput_tot = np.convolve(thrput_tot, np.ones(n_ave)/n_ave)
    for i in range(1,n_ave):
        smoothed_thrput_tot[i-1] = float(sum(thrput_tot[0:i]))/len(thrput_tot[0:i])
    smoothed_thrput_tot = smoothed_thrput_tot[0:len(thrput_tot)]
    return smoothed_thrput_tot

def main():
    "Create network and run downward spiral effect experiment"
    
    # Reset to known state
    topo = StarTopo(n=args.n, bw_host=args.bw_host,
                    delay='%sms' % args.delay,
                    bw_net=args.bw_net, maxq=args.maxq)
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    dumpNodeConnections(net.hosts)
    net.pingAll()
    
    #  verify latency and bandwidth of links in the topology 
    # just created.
    for i in range(1,args.n):
        latency = verify_latency(net,i)
        if latency:
            cprint ("RTT for from host%d to server is verified to be correct" % i,'green')
        else:
            cprint ("RTT for from host%d to server is wrong" % i, 'red')


    verify_bw_tol = 0.98
    bw_check = verify_bandwidth(net,1,3)
    print bw_check
    if bw_check/args.bw_net >= verify_bw_tol:
        cprint ("Bandwidth of bottleneck link is verified to be correct",'green')
    else:
        cprint ("Bandwidth of bottleneck link is wrong",'red')

    # start the webserver on h3
    start_tcpprobe()
    start_webserver(net)
    
    cprint("Starting experiment", "green")

    #    iface='s0-eth2'

    start = time()
    #Initial buffer occupancy is 0
    buffer_occu = 0
    #Actual throughput measurements by dividing file size by fetch time
    thrput_tot = []
    #Time passed
    tot_time = []  # initial time is zero
    #Debug variable
    adjusted_thrput_tot = []
    #Flag indicating to start of the video streaming
    is_start = 1
    #Flag indicating to start of the competing flow
    comp_flow = 0
    #list of rates requested by the client
    rate_request_tot = []
    #list of buffer occupancy at various times
    buffer_occu_tot = []
    
    
    h1 = net.getNodeByName('h1')
    h2 = net.getNodeByName('h2')
    h3 = net.getNodeByName('h3')
    #    CLI(net)

    #    context = h1.popen("curl_init()",shell=True)
    
    while True:

        if (is_start == 1):
            # Indicate that the first segemnt is downloaded
            is_start = 0
        else:
            # Wait if buffer does not have space to accommodate new flow
            wait_time = get_wait_time(buffer_available)
            #            print wait_time
            sleep(int(wait_time))
            # check buffer occupancy
            buffer_occu = max(buffer_occu - wait_time,0)

        #request rate based on estimated throughput
        rate_request = choose_rate(thrput_tot,tot_time)
        file_size = rate_request*SEGMENT_SIZE
        #        fetch_time = float(get_file(net,file_size))  #DEBUG code
        #Start the download of file using Byte range requests to support non-discrete rates
        fetch_time = h1.cmd("curl --keepalive-time %d -r 0-%d -o /dev/null -s -w %s %s/%s " % (args.time,int((file_size*1e6)/8)-1,"%{time_total}",h3.IP(),"http/ready.html"))
        fetch_time = float(fetch_time)
        #        adjusted_thrput_tot.append(float(get_adjusted_thrput(thrput_tot,FILTER_LEN)))

        #Query the buffer for its space
        buffer_available = check_buffer(rate_request,file_size,fetch_time,buffer_occu)
        buffer_occu = PLAYBACK_BUFFER - buffer_available
        #calculate instantaneous throughput
        inst_thrput = (file_size/fetch_time)
        thrput_tot.append(inst_thrput)
    
        rate_request_tot.append(rate_request)
    
        now = time()
        delta = now - start
        # time passed
        tot_time.append(delta)
    
        #        print ("buffer occupancy = %f" %(buffer_occu) )
        #        print ("buffer available = %f" %(buffer_available) )

        #When to start the competing flow. Iperf provides competing flow
        if (comp_flow == 0):
            if (delta > comp_flow_start_time):
                print "Its time to start competing flow"
                # Alternately, HTTP may have provided competing flow
                #                h2.popen("curl -o /dev/null -s %s/%s &" % (h3.IP(),"http/compflow.html"),shell=True)
                start_iperf(net)
                comp_flow = 1
    
        if delta > args.time:
            break
        print "%.1fs passed..." % (delta)
    
    # Shut down iperf processes
    os.system('killall -9 iperf')
    net.stop()
    Popen("pgrep -f webserver.py | xargs kill -9", shell=True).wait()
    Popen("killall -9 top bwm-ng tcpdump cat mnexec", shell=True).wait()
    stop_tcpprobe()

    # We can estimate the competing flow bandwidth using periodic bandwidth updates iperf flow gives
    compflow_bw = []
    with open('compflow_download_speed_server_side.txt') as f:
        for line in f:
            #            print line
            compflow_bw.append(float("%s"%(line[-15:-11])))
            #    print compflow_bw

    #Create, smooth, and save the plots
    smoothed_compflow_bw =  smooth(compflow_bw,n_ave)
    smoothed_thrput_tot = smooth(thrput_tot,n_ave)

    plot_inst_thr(tot_time,smoothed_thrput_tot,rate_request_tot,smoothed_compflow_bw)

    #Code that may have been used for persistent HTTP connection
    #        context = h1.cmd("curl_init()")
    #        h1.cmd("curl_setopt(%s, CURLOPT_URL, %s/%s)"%(context,h3.IP(),"http/ready.html"))
    #        h1.cmd("curl_exec(%s)"%(context))
    #        h1.cmd("curl_close(%s)"%(context))





if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf; mn -c")

