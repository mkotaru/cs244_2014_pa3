#!/bin/bash

# Exit on any failure
set -e

# Check for uninitialized variables
set -o nounset

ctrlc() {
	killall -9 python
	mn -c
	exit
}

trap ctrlc SIGINT

start=`date`
exptid=`date +%b%d-%H:%M`

plotpath=util
iperf=~/iperf-patched/src/iperf


# Code to accept input arguments
EXPECTED_ARGS=2

if [ $# -ne $EXPECTED_ARGS ]
then
echo "If you want to use alternate values for parameters, Usage: sudo ./run.sh <BUFFER_SIZE_AT_BOTTLENECK_LINK> <CONSERVATISM> "
echo "Running for all the parametrs in the report  "
#declare -i conservative=0
#declare -i qsize=120
#exit
dir_def='results'
rootdir=$dir_def-$exptid
dir=$rootdir
for qsize in 20 75 120; do
for conservative in 0 0.7 1; do
    # Start the experiment
    python spiral.py --bw-host 100 \
        --bw-net 5 \
        --delay 1 \
        --dir $dir \
        -n 3 \
        --conservative $conservative\
        --maxq $qsize\
        --time 900
done
done
fi

if [ $# -eq $EXPECTED_ARGS ]
then
echo "Running with values BUFFER_SIZE_AT_BOTTLENECK_LINK = " $1 ", CONSERVATISM = " $2
qsize=$1
conservative=$2

dir_def='results'
rootdir=$dir_def-$exptid
dir=$rootdir
# Start the experiment
    python spiral.py --bw-host 100 \
        --bw-net 5 \
        --delay 1 \
        --dir $dir \
        -n 3 \
        --conservative $conservative\
        --maxq $qsize\
        --time 900

fi



echo "Started at" $start
echo "Ended at" `date`
