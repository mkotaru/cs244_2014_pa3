To recreate all the results in the paper using a single command, use:
sudo ./run.sh

(You may require to make the run.sh file executable using command: chmod u+x run.sh)

Specifically, sudo ./run.sh recreates results for each combination of <BUFFER_SIZE_AT_BOTTLENECK> in [20 75 120] and <CONSERVATISM> in [0 0.7 1].

Each expeirment takes runs for 15 minutes. So, running all the experiments takes around 2 and 1/2 hours.

To recreate individual plots mentioned in the report, and to play with different parameters of interest in the setup, one can optionally add the following arguments:
sudo ./run.sh <BUFFER_SIZE_AT_BOTTLENECK> <CONSERVATISM> 

<BUFFER_SIZE_AT_BOTTLENECK> is buffersize at bottleneck link in kbits. For example, the input argument would be 120 to make the buffer size at bottleneck link 120 kbit.
<CONSERVATISM> is the ratio between video bit rate requested and the perceived throughput for non-discrete video bitrate. For example, if conservatism is 0.6, perceived throughput of 2Mbps results in requesting rate 1.2 Mbps. If <CONSERVATISM> = 0, then commercial video streaming service client is used. 

For example, to just recreate the figure in the paper (Figure 2 in the report), use the command:
sudo ./run.sh 120 0 

The results are stored in results-(date+time) directory. Each file is named after biffersize at bottleneck link and conservatism. For example if buffer size is 20 kbit, and conservatism is zero, i.e., use commercial video streaming service client, then the figure is stored as results-(date-time)/conservativeRatio0.0-qsize20.png

If the number of input arguments is anything other than 2, all the figures in the report are recreated.

For convenience, the results in the report are present in 'results' directory. fig1.png corresponds to figure 1 in the report and so on. 